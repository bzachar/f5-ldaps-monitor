# f5-ldaps-monitor
A custom LDAPS health monitor for F5. Ansible has a bug in bigip_ldap_monitor to configure LDAPS using the builtin LDAP
monitors as with every second run it disables TLS (sets the secure attribure to none instead of ssl) which causes the 
health monitors failing for all pool members.

# Usage
```
$ /tmp/f5-ldaps-monitor --help
The check expects two mandatory arguments:
1. ip address (IPv4-mapped IPv6 address for IPv4 address, e.g. ":ffff:a.b.c.d")
2. tcp port number

The rest of the program is controlled by environment variables (defaults in parenthesis):
* USERDN:    dn of the user to use for the bind [REQUIRED]
* PASSWORD:  password of the userdn to bind [REQUIRED]
* DEBUG:     when set to anything than 0 enables debugging output to syslog (0)

```

# DEBUG
Change the DEBUG variable to 1 and on the F5 box:
```bash
$ journalctl -f
```