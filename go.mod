module gitlab.com/bzachar/f5-ldaps-monitor

go 1.15

require (
	github.com/go-ldap/ldap/v3 v3.4.1
	gopkg.in/ldap.v2 v2.5.1 // indirect
)
