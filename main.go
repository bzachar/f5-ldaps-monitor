package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"log/syslog"
	"os"
	"strconv"

	"github.com/go-ldap/ldap/v3"
)

const (
	helpText = `The check expects two mandatory arguments:
1. ip address (IPv4-mapped IPv6 address for IPv4 address, e.g. ":ffff:a.b.c.d")
2. tcp port number

The rest of the program is controlled by environment variables (defaults in parenthesis):
* USERDN:    dn of the user to use for the bind [REQUIRED]
* PASSWORD:  password of the userdn to bind [REQUIRED]
* DEBUG:     when set to anything than 0 enables debugging output to syslog (0)`
)

var (
	hasDebug     = false
	syslogWriter io.Writer
)

func init() {
	var err error
	// Set custom help message
	flag.Usage = func() {
		_, err = fmt.Fprintln(os.Stderr, helpText)
		if err != nil {
			// Do nothing
		}
		os.Exit(127)
	}

	// extract commandline arguments
	flag.Parse()
	if flag.NArg() != 2 {
		log.Fatalf("Got %d commandline arguments, expected exactly two", flag.NArg())
	}

	// enable syslog for debug runs
	if os.Getenv("DEBUG") != "" && os.Getenv("DEBUG") != "0" {
		hasDebug = true
		syslogWriter, err = syslog.New(syslog.LOG_INFO|syslog.LOG_USER, "ldaps-monitor")
		// if syslog fails, silently discard debug output
		if err != nil {
			//fmt.Fprintf(os.Stderr, "Unable to create syslog writer: %s\n", err.Error())
			syslogWriter = ioutil.Discard
		}
	}

}

// DebugLog is printf for syslog or a NOP, depending on the global hasDebug variable
func DebugLog(format string, args ...interface{}) {
	if hasDebug {
		msg := fmt.Sprintf(format, args...)
		_, err := syslogWriter.Write([]byte(msg))
		if err != nil {
			// Do nothing
		}
	}
}

func main() {
	tlsConfig := tls.Config{
		InsecureSkipVerify: true,
	}

	// Wrap server IP into [] as F5 provides IPv4 mapped IPv6 address like "::ffff:172.21.0.6"
	server := fmt.Sprintf("[%s]", os.Args[1])
	port, err := strconv.Atoi(os.Args[2])
	if err != nil {
		DebugLog("The following argument has been provided as port that cannot be converted to integer: %s", os.Args[2])
		os.Exit(1)
	}
	DebugLog("Host: %s, Port: %d", server, port)

	l, err := ldap.DialTLS("tcp", fmt.Sprintf("%s:%d", server, port), &tlsConfig)
	if err != nil {
		DebugLog("Error connecting to %s on port %d: %s", server, port, err.Error())
		os.Exit(1)
	}
	defer l.Close()
	DebugLog("Connected to %s on port %d using TLS", server, port)

	userdn := os.Getenv("USERDN")
	password := os.Getenv("PASSWORD")
	if userdn == "" || password == "" {
		DebugLog("USERDN and PASSWORD env variables must be provided")
		os.Exit(1)
	}
	err = l.Bind(userdn, password)
	if err != nil {
		DebugLog("Could not authenticate to LDAP server with the provided credentials")
		os.Exit(1)
	}

	// success
	fmt.Println("UP")
	DebugLog("Authentication was successful using the provided User DN and password")
}
